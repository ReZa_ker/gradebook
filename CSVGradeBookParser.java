package src;

import java.util.*;
import java.io.*;
import au.com.bytecode.opencsv.*;

public class CSVGradeBookParser implements GradeBookParser {
	private String filename;
	
	public CSVGradeBookParser(String filename) {
		this.filename = filename;
	}
	
	public List<StudentInfo> parse() {
		ArrayList<StudentInfo> sia = new ArrayList<StudentInfo>();
		
		try {
			CSVReader reader = new CSVReader(new FileReader(filename));
			String[] header = reader.readNext();
		    String[] nextLine;
		    while ((nextLine = reader.readNext()) != null) {
				StudentInfo si = new StudentInfo(nextLine[0]);
				for (int i = 1; i < nextLine.length; i++)
					si.setScore(header[i], Double.parseDouble(nextLine[i]));
				sia.add(si);
		    }
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		
		return sia;
	}
}