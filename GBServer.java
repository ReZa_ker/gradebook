package src;

import java.io.*;
import java.net.*;
import java.util.*;
import com.sun.net.httpserver.*;
import ir.ramtung.coolserver.*;

class ShowStudentInfo extends ServiceHandler {
    public void execute(PrintWriter out) {
    	StudentInfo si = GradeBook.gb().getItem(params.get("name"));
        new Page("./templates/student.html")
            .subst("name", params.get("name"))
            .subst("midterm", si.getScore("midterm") + "")
            .subst("final", si.getScore("final") + "")
            .subst("homeworks", si.getScore("homeworks") + "")
            .subst("quizzes", si.getScore("quizzes") + "")
            .writeTo(out);
    }
}

class PercentPassed extends ServiceHandler {
    public void execute(PrintWriter out) {
    	Map<String, Double> weights = new HashMap<String, Double>();
    	for (String item : params.keySet())
    		weights.put(item, Double.parseDouble(params.get(item)));
    	double percentPassed = GradeBook.gb().percentPassed(weights);
    	new Page("./templates/passed.html")
    		.subst("percent", percentPassed + "")
    		.writeTo(out);
    }
}

public class GBServer {
	public static void main(String[] args) throws IOException {
        // gb = new src.GradeBook(new src.CSVGradeBookParser("grades.csv"));
        GradeBook.init(new XMLGradeBookParser("grades.xml"));

        HttpServer server = HttpServer.create(new InetSocketAddress(9090), 0);
        server.createContext("/docs", new FileHandler());
        server.createContext("/student", new ShowStudentInfo());
        server.createContext("/passed", new PercentPassed());
        server.start();
	}
}