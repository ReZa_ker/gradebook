package src;

import java.util.*;

public class GradeBook {
	private static GradeBook theGradeBook = null;
	
	public static GradeBook gb() {
		return theGradeBook;
	}

	public static void init(GradeBookParser parser) {
		theGradeBook = new GradeBook(parser);
	}

	private Map<String, StudentInfo> students;
	
	private GradeBook(GradeBookParser parser) {
		students = new HashMap<String, StudentInfo>();
		for (StudentInfo si : parser.parse())
			students.put(si.getName(), si);
	}

	public StudentInfo getItem(String name) {
		return students.get(name);
	}

	public Collection<StudentInfo> getAll() {
		return students.values();
	}
	
	public double percentPassed(Map<String, Double> weights) {
		int passCount = 0;
		for (StudentInfo si : students.values())
			passCount += si.getTotal(weights) < 10 ? 0 : 1;

		return passCount * 100.0 / students.size();
	}
}