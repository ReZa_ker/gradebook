package src;

import java.util.*;

public class StudentInfo {
	private String name;
	private Map<String, Double> scores;
	
	public StudentInfo(String studentName) {
		name = studentName;
		scores = new HashMap<String, Double>();
	}

	public void setScore(String item, double score) {
		scores.put(item, score);
	}

	public String getName() {
		return name;
	}

	public double getScore(String item) {
		return scores.get(item);
	}

	public double getTotal(Map<String, Double> weights) {
		double total = 0;
		for (String item : weights.keySet())
			total += weights.get(item) * scores.get(item);
		return total;
	}
}