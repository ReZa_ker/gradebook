package src;

import java.util.*;

public interface GradeBookParser {
	public List<StudentInfo> parse();
}