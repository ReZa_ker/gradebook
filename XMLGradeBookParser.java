package src;

import javax.xml.parsers.*;
import org.xml.sax.helpers.*;
import org.xml.sax.*;
import java.io.*;
import java.util.*;

public class XMLGradeBookParser implements GradeBookParser {
	private String filename;
	
	public XMLGradeBookParser(String filename) {
		this.filename = filename;
	}

	public List<StudentInfo> parse() {
		final ArrayList<StudentInfo> sia = new ArrayList<StudentInfo>();

		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			
			sp.parse(new File(filename), new DefaultHandler() {
					StudentInfo si;
				
			        public void startElement(String uri, String lName, String qName, Attributes attrs) {
						if (qName.equals("Student"))
							si = new StudentInfo(attrs.getValue("name"));
						else if (qName.equals("Grade"))
							si.setScore(attrs.getValue("item"), Double.parseDouble(attrs.getValue("value")));
					}

			        public void endElement(String uri, String lName, String qName) {
						if (qName.equals("Student")) {
							sia.add(si);
						}
					}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return sia;
	}
}